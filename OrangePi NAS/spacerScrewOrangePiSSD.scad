/*
|   --- scad file for enez project ---
|   
|   Copyright finizi 2019
|   my.inizisoft.net/grav/enez
|
|     This program is free software: you can redistribute it and/or modify
|      it under the terms of the Creative Commons Attribution-ShareAlike 4.0 International 
|      (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/.
|     You are free to:
|        Share : copy and redistribute the material in any medium or format
|        Adapt : remix, transform, and build upon the material
|      for any purpose, even commercially.
|     This license is acceptable for Free Cultural Works.
|     The licensor cannot revoke these freedoms as long as you follow the license terms.
|
|     Code sous licence (CC BY-SA 4.0) :
|      Creative Commons Attribution - Partage dans les M�mes Conditions 4.0 International
|
*/

/*
**  Nuts and washers for OrangePi NAS & SSD
*/

extDiam = 6.0;
spaceInnerDiam = 3.3;
screwInnerDiam = 2.9;
spaceH = 2.8;
screwH = 23.02;
piH = 0.7;



/* Hexagonal foot between NAS and DIN support plate
*/
difference() {
  cylinder(d=extDiam, h=screwH, $fn=6);
  cylinder(d=screwInnerDiam, h=screwH, $fn=120);
}

/* Spacer washer between SSD and NAS. 
** Screws pass throuht this washer to fix SSD in the hexagonal foot.
*/
translate([10, 0, 0]) difference() {
  cylinder(d=extDiam, h=spaceH, $fn=120);
  cylinder(d=spaceInnerDiam, h=spaceH, $fn=120);
}

/* Spacer washers between pi and hexagonal spacer to avoid distorting the NAS
*/
translate([20, 0, 0]) difference() {
  cylinder(d=extDiam, h=piH, $fn=120);
  cylinder(d=spaceInnerDiam, h=piH, $fn=120);
}
translate([30, 0, 0]) difference() {
  cylinder(d=extDiam, h=piH, $fn=120);
  cylinder(d=spaceInnerDiam, h=piH, $fn=120);
}


